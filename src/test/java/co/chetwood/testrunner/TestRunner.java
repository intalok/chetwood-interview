package co.chetwood.testrunner;


import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        publish = true,
        features = "src/test/java/co/chetwood/features",
        glue = {"co.chetwood.stepdefinitions"},
        plugin = {"pretty", "html:target/cucumber/report.html"},
        monochrome=true,
        tags = "@testcase",
        dryRun=false
)

public class TestRunner {




}
