package co.chetwood.wrapper;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class SeleniumWrapper {

    WebDriver driver;
    WebDriverWait wait;
    JavascriptExecutor jse;


    public SeleniumWrapper (WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        jse = (JavascriptExecutor) driver;
    }

    public void openUrl(String url) {
        driver.navigate().to(url);
    }

    public void waitAndClick(By by) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(by)).click();
    }

    public String getText(By by) {
        WebElement webElement = wait.until(ExpectedConditions.visibilityOfElementLocated(by));
        return webElement.getText();
    }
}
