@smoke
Feature: Test the request features on the Home Page


  @testcase
  Scenario: Should be able to view information about LiveLend interest and amount repayable
    Given Go to LiveLend "HomePage"
    When  move the borrow amount slider from £1000 to £11000
    Then  verify monthly repayment and repayable figures will change to correct amounts

#  Scenario: Should be able to view information about LiveLend interest and amount repayable
#    Given Go to LiveLend "HomePage"
#    When  move the loan term slider from 12 months to 60 months
#    Then  verify monthly repayment and repayable figures will change to correct amounts
#
#
#  Scenario: Should be able to view information about LiveLend interest and amount repayable
#    Given Go to LiveLend "HomePage"
#    When  click on the "Get My Quote" button
#    Then  verify page is redirected to "https://secure2.livelend.co/customer/details/"







