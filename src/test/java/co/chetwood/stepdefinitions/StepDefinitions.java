package co.chetwood.stepdefinitions;

import co.chetwood.DriverFactory.WebDriverFactory;
import co.chetwood.pages.HomePage;
import co.chetwood.pages.Setup;
import io.cucumber.java8.En;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

public class StepDefinitions implements En {

    WebDriver driver = null;
    HomePage homePage;

    public StepDefinitions() {

        Given("Go to LiveLend {string}", (String pageName) -> {

            driver = WebDriverFactory.getWebDriver();
            homePage = new HomePage(driver);

            String homepageUrl = Setup.configuration.getProperty("home.page.url");
            homePage.openUrl(homepageUrl);
            homePage.clickOnAcceptCookies();
        });

        When("move the borrow amount slider from £{int} to £{int}", (Integer arg0, Integer arg1) -> {
            homePage.moveslider("6500");
        });

        Then("verify monthly repayment and repayable figures will change to correct amounts", () -> {
            String actualMonthlyRepayment = homePage.verifyMonthlyRepayment();
            Assert.assertEquals(actualMonthlyRepayment, "£238.87");
        });

        /*When("^move the loan term slider from (\\d+) months to (\\d+) months$", (Integer arg0, Integer arg1) -> {
            System.out.println("move the loan term slider from " + arg0 + " months to " + arg1 + " months");
        });

        When("^click on the \"([^\"]*)\" button$", (String arg0) -> {
            System.out.println("click on the " + arg0 + " button");
        });

        Then("^verify page is redirected to \"([^\"]*)\"$", (String arg0) -> {
            System.out.println("verify page is redirected to " + arg0);
        });

        Then("^verify click on and navigate to links displayed in the header$", () -> {
            System.out.println("verify click on and navigate to links displayed in the header");
        });*/


    }
}
