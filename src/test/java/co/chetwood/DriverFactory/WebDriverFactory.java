package co.chetwood.DriverFactory;

import co.chetwood.pages.Setup;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;

public class WebDriverFactory {

    public static WebDriver getWebDriver() {

        WebDriver driver;
        if (Setup.configuration.getProperty("browser").equalsIgnoreCase("chrome")) {
            ChromeOptions chromeOptions = new ChromeOptions();
            chromeOptions.addArguments("start-maximized");
            //Start Headless, etc

            WebDriverManager.chromedriver().setup();
            driver = new ChromeDriver(chromeOptions);
        } else {

            WebDriverManager.edgedriver().setup();
            driver = new EdgeDriver();
        }
        return driver;
    }
}
