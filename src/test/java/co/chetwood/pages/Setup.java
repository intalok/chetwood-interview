package co.chetwood.pages;

import io.cucumber.java.Before;
import io.cucumber.java.BeforeAll;
import org.openqa.selenium.WebDriver;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Setup {

    public static Properties configuration ;
    public static FileInputStream fisConfigurationFile ;

    WebDriver driver = null;

    static {
        try {
            fisConfigurationFile = new FileInputStream("src/test/resources/configuration.properties");
            configuration = new Properties();
            configuration.load(fisConfigurationFile);

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
