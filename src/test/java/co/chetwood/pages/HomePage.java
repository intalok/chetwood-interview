package co.chetwood.pages;

import com.google.common.util.concurrent.Uninterruptibles;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.concurrent.TimeUnit;

public class HomePage extends BasePage{

    //By Locators
    By byAcceptButton = By.xpath(".//button[text()='Accept']");
    By byLoanAmountSlider = By.id("loanAmount");
    By byMonthlyRepaymentText = By.xpath(".//div[@id='monthly-result']//b[@class='content']/span");

    private WebDriver driver = null;

    public HomePage(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public void openUrl(String url) {
        seleniumWrapper.openUrl(url);
    }

    public void moveslider(String amount) {

        switch (amount) {
            case "6500":
                seleniumWrapper.waitAndClick(byLoanAmountSlider);
        }

        /* =========== JUST FOR REFERENCE =====================
        By byLoanAmountSlider = By.id("loanAmount");
        WebElement weLoanAmountSlider = driver.findElement(byLoanAmountSlider);
        System.out.println(weLoanAmountSlider.getLocation().x);
        System.out.println(weLoanAmountSlider.getSize().width);

        Uninterruptibles.sleepUninterruptibly(2,TimeUnit.SECONDS);
        Actions actions = new Actions(driver);
        actions.moveToElement(weLoanAmountSlider).moveByOffset(-150, 0).click().build().perform();
        */
    }

    public void clickOnAcceptCookies() {
        try {
            seleniumWrapper.waitAndClick(byAcceptButton);
        } catch (Exception e) {
            //Do Nothing
        }

    }

    public String verifyMonthlyRepayment() {
        Uninterruptibles.sleepUninterruptibly(500, TimeUnit.MILLISECONDS);
        return seleniumWrapper.getText(byMonthlyRepaymentText);
    }
}
