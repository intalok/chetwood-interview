package co.chetwood.pages;

import co.chetwood.wrapper.SeleniumWrapper;
import org.openqa.selenium.WebDriver;

public abstract class BasePage {

    protected SeleniumWrapper seleniumWrapper = null;
    private WebDriver driver = null;

    public BasePage(WebDriver driver) {
        this.driver = driver;
        seleniumWrapper = new SeleniumWrapper(driver);
    }




}
